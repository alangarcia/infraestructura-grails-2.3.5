package com.rs.catalogo

/**
 * RsCatTipoAsentamiento
 * A domain class describes the data object and it's mapping to the database
 */
class RsCatTipoAsentamiento {

    String  claveTipoAsentamiento
    String  nombreTipoAsentamiento

    /* Default (injected) attributes of GORM */
//	Long	id
//	Long	version

    /* Automatic timestamping of GORM */
//	Date	dateCreated
//	Date	lastUpdated

//	static	belongsTo	= []	// tells GORM to cascade commands: e.g., delete this object if the "parent" is deleted.
//	static	hasOne		= []	// tells GORM to associate another domain object as an owner in a 1-1 mapping
//	static	hasMany		= []	// tells GORM to associate other domain objects for a 1-n or n-m mapping
//	static	mappedBy	= []	// specifies which property should be used in a mapping 

    static mapping = {
    }

    static constraints = {
        claveTipoAsentamiento(size:2..15, unique: true, nullable: false, blank: false)
        nombreTipoAsentamiento(size:5..50, unique: true, nullable: false, blank: false)
    }

    String toString() {
        "${nombreTipoAsentamiento}"
    }


    /*
     * Methods of the Domain Class
     */
//	@Override	// Override toString for a nicer / more descriptive UI 
//	public String toString() {
//		return "${name}";
//	}
}
