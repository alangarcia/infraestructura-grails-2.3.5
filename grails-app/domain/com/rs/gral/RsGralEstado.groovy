package com.rs.gral

/**
 * RsGralEstado
 * A domain class describes the data object and it's mapping to the database
 */
class RsGralEstado {

    String  cveEstado
    String  nombreEstado
    String  aliasEstado
    String 	escudoEstado

    SortedSet ciudad

    static hasMany = [ delegacionMunicipio : RsGralDelegacionMunicipio]
    static belongsTo	= [pais: RsGralPais]	// tells GORM to cascade commands: e.g., delete this object if the "parent" is deleted.

    static constraints = {
        cveEstado size:2..20, unique: true,nullable: false, blank: false
        nombreEstado size:3..50, unique: true,nullable: false, blank: false
        aliasEstado 	nullable: true
        escudoEstado	nullable: true
    }

    String toString() {
        "${nombreEstado}"
    }

    static mapping = {
        sort "nombreEstado"
    }



    /* Default (injected) attributes of GORM */
//	Long	id
//	Long	version

    /* Automatic timestamping of GORM */
//	Date	dateCreated
//	Date	lastUpdated

//	static	belongsTo	= []	// tells GORM to cascade commands: e.g., delete this object if the "parent" is deleted.
//	static	hasOne		= []	// tells GORM to associate another domain object as an owner in a 1-1 mapping
//	static	hasMany		= []	// tells GORM to associate other domain objects for a 1-n or n-m mapping
//	static	mappedBy	= []	// specifies which property should be used in a mapping 

    /*
     * Methods of the Domain Class
     */
//	@Override	// Override toString for a nicer / more descriptive UI 
//	public String toString() {
//		return "${name}";
//	}
}
